﻿# jpress-addon-LinkManage
![链接列表](https://images.gitee.com/uploads/images/2020/0321/165357_db7ab3da_2286314.png "~$`]YMY(L[6{XMU77U~EW%8.png")
![添加链接](https://images.gitee.com/uploads/images/2020/0321/165436_02b85a29_2286314.png "YSCJU@T]0XS1B_$(XV{H45P.png")
![分类列表](https://images.gitee.com/uploads/images/2020/0321/165522_c68710d1_2286314.png "MAD8Y@P``TD3IOUQZJ1[M2D.png")
![添加分类](https://images.gitee.com/uploads/images/2020/0321/165555_fd7a0d34_2286314.png "D)T_WMV~)RQWEZ]5~O7}K0B.png")
#### 介绍
Jpress 的一款很强大的友情链接管理插件
链接管理
分类管理
首页展示
一键获取链接信息
我的博客：http://ucuser.cn
#### 软件架构
软件架构说明


#### 安装教程

1.  下载
2.  在jpress 后台 安装插件处 上传 `cn.ucuser.linkManage.jar` 
3.  启用
4.  开始使用

#### 使用说明

## 食用方法

在html页面 只有 `网站状态` 是 `通过`的才可以展示

```
 #LinkManagesTypeAll()
		#for(linkType : LinkTypes)
			分类：#(linkType.name)
		#LinkList(linkType.id)
				#for(link : Links)
					链接：#(link.title)
			#end
		#end
		#end
#end
```

### link下的所有属性

```
id
title           标题
description     描述
logo            logo图片
url             链接
time            添加时间
name            网站作者名称
email           网站联系邮箱
remark          备注
```

### linkType下的所有属性

```
id
name            分类名
description     分类描述
icon            分类图标,你可以食用ico图片也可也食用图片地址，按模板需求来
```

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
