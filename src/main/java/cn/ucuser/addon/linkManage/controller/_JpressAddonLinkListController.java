/**
 * Copyright (c) 2016-2020, Michael Yang 杨福海 (fuhai999@gmail.com).
 * <p>
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.ucuser.addon.linkManage.controller;

import com.jfinal.aop.Inject;
import com.jfinal.kit.Ret;
import com.jfinal.plugin.activerecord.Page;

import io.jboot.db.model.Columns;
import io.jboot.utils.StrUtil;
import io.jboot.web.controller.annotation.RequestMapping;
import io.jboot.web.validate.EmptyValidate;
import cn.ucuser.addon.linkManage.model.JpressAddonLinkList;
import cn.ucuser.addon.linkManage.service.JpressAddonLinkListService;
import io.jboot.web.validate.Form;
import io.jpress.commons.utils.CommonsUtils;
import io.jpress.core.menu.annotation.AdminMenu;
import io.jpress.web.base.AdminControllerBase;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


@RequestMapping(value = "/admin/LinkManage/jpress_addon_link_list", viewPath = "/")
public class _JpressAddonLinkListController extends AdminControllerBase {

    @Inject
    private JpressAddonLinkListService service;

    @AdminMenu(text = "链接管理", groupId = "LinkManage")
    public void index() {
        Columns columns = new Columns();
        if (!StringUtils.isBlank(getPara("type"))) {
            columns.eq("type",getPara("type"));
        }
        if (!StringUtils.isBlank(getPara("title"))) {
            columns.like("title","%"+getPara("title")+"%");
        }
        if (!StringUtils.isBlank(getPara("state"))) {
            columns.eq("state",getPara("state"));
        }
        Page<JpressAddonLinkList> entries=service.paginateByColumns(getPagePara(), 10,columns);

        setAttr("page", entries);
        render("views/jpress_addon_link_list_list.html");
    }


    public void getICO(){
        Map<String,Object> map = new HashMap<>();
        Document doc = null;
        try {
            String icon = "";
            String description = "";
            doc = Jsoup.connect(get("url")).get();
            String title = doc.title();//标题
            Elements link = doc.select("link[rel=icon]");
            if (link.size()>0) {
                icon = link.get(0).attr("href");//icon图标
            }
            Elements descriptionEle = doc.select("meta[name=description]");
            if (descriptionEle.size()>0) {
                description = descriptionEle.get(0).attr("content");
            }
            map.put("icon",icon);
            map.put("title",title);
            map.put("description",description);
        } catch (IOException e) {
            e.printStackTrace();
        }
        setAttrs(map);
        renderJson(map);
    }


    public void edit() {
        int entryId = getParaToInt(0, 0);
        JpressAddonLinkList entry = entryId > 0 ? service.findById(entryId) : null;
        setAttr("jpressAddonLinkList", entry);
        render("views/jpress_addon_link_list_edit.html");
    }

    public void doSave() {
        JpressAddonLinkList entry = getModel(JpressAddonLinkList.class,"jpressAddonLinkList");

        if (StrUtil.isBlank(entry.getTitle())) {
            renderJson(Ret.fail().set("msg","对不起标题不能是空"));
            return;
        }
        if (StrUtil.isBlank(entry.getUrl())) {
            renderJson(Ret.fail().set("msg","对不起链接不能是空"));
            return;
        }
        if (StrUtil.isBlank(entry.getType().toString())) {
            renderJson(Ret.fail().set("msg","对不起分类不能是空"));
            return;
        }
        if (StrUtil.isBlank(entry.getTime().toString())) {
            renderJson(Ret.fail().set("msg","对不起时间不能是空"));
            return;
        }
        if (StrUtil.isBlank(entry.getState().toString())) {
            renderJson(Ret.fail().set("msg","对不起状态不能是空"));
            return;
        }

        service.saveOrUpdate(entry);
        renderJson(Ret.ok().set("id", entry.getId()));
    }

    public void doDel() {
        Long id = getIdPara();
        render(service.deleteById(id) ? Ret.ok() : Ret.fail());
    }

    @EmptyValidate(@Form(name = "ids"))
    public void doDelByIds() {
        service.batchDeleteByIds(getParaSet("ids").toArray());
        renderOkJson();
    }
}