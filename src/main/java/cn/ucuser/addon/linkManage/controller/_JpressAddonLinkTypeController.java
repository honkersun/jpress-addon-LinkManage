/**
 * Copyright (c) 2016-2020, Michael Yang 杨福海 (fuhai999@gmail.com).
 * <p>
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.ucuser.addon.linkManage.controller;

import com.jfinal.aop.Inject;
import com.jfinal.kit.Ret;
import com.jfinal.plugin.activerecord.Page;

import io.jboot.utils.StrUtil;
import io.jboot.web.controller.annotation.RequestMapping;
import io.jboot.web.validate.EmptyValidate;
import cn.ucuser.addon.linkManage.model.JpressAddonLinkType;
import cn.ucuser.addon.linkManage.service.JpressAddonLinkTypeService;
import io.jboot.web.validate.Form;
import io.jpress.core.menu.annotation.AdminMenu;
import io.jpress.web.base.AdminControllerBase;


@RequestMapping(value = "/admin/LinkManage/jpress_addon_link_type", viewPath = "/")
public class _JpressAddonLinkTypeController extends AdminControllerBase {

    @Inject
    private JpressAddonLinkTypeService service;

    @AdminMenu(text = "分类管理", groupId = "LinkManage")
    public void index() {
        Page<JpressAddonLinkType> entries=service.paginate(getPagePara(), 10);
        setAttr("page", entries);
        render("views/jpress_addon_link_type_list.html");
    }


    public void edit() {
        int entryId = getParaToInt(0, 0);
        JpressAddonLinkType entry = entryId > 0 ? service.findById(entryId) : null;
        setAttr("jpressAddonLinkType", entry);
        render("views/jpress_addon_link_type_edit.html");
    }

    public void doSave() {
        JpressAddonLinkType entry = getModel(JpressAddonLinkType.class,"jpressAddonLinkType");

        if (StrUtil.isBlank(entry.getName())) {
            renderJson(Ret.fail().set("msg","对不起分类名称不能是空"));
            return;
        }

        service.saveOrUpdate(entry);
        renderJson(Ret.ok().set("id", entry.getId()));
    }

    public void doDel() {
        Long id = getIdPara();
        render(service.deleteById(id) ? Ret.ok() : Ret.fail());
    }

    @EmptyValidate(@Form(name = "ids"))
    public void doDelByIds() {
        service.batchDeleteByIds(getParaSet("ids").toArray());
        renderOkJson();
    }
}