package cn.ucuser.addon.linkManage.directive;

import cn.ucuser.addon.linkManage.model.JpressAddonLinkList;
import cn.ucuser.addon.linkManage.service.JpressAddonLinkListService;
import com.jfinal.aop.Inject;
import com.jfinal.template.Env;
import com.jfinal.template.io.Writer;
import com.jfinal.template.stat.Scope;
import io.jboot.db.model.Columns;
import io.jboot.web.directive.annotation.JFinalDirective;
import io.jboot.web.directive.base.JbootDirectiveBase;
import org.apache.commons.lang3.StringUtils;

import java.util.List;

/**
 * @program: parent
 * @description: 查找指定分类下的链接
 * @author: Pan
 * @create: 2020-03-21 12:51
 **/
@JFinalDirective("LinkList")
public class LinkManageListDirective extends JbootDirectiveBase {

    @Inject
    private JpressAddonLinkListService service;
    @Override
    public void onRender(Env env, Scope scope, Writer writer) {
        Object type = getPara("tid", scope);//获取到分类id
        Columns c = new Columns();
        c.eq("type",type);
        c.eq("state",1);
        List<JpressAddonLinkList> listByColumns = service.findListByColumns(c);
        scope.setLocal("Links", listByColumns);
        renderBody(env, scope, writer);
    }

    @Override
    public boolean hasEnd() {
        return true;
    }
}
