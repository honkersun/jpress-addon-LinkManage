package cn.ucuser.addon.linkManage.directive;

import cn.ucuser.addon.linkManage.model.JpressAddonLinkType;
import cn.ucuser.addon.linkManage.service.JpressAddonLinkTypeService;
import com.jfinal.aop.Inject;
import com.jfinal.template.Env;
import com.jfinal.template.io.Writer;
import com.jfinal.template.stat.Scope;
import io.jboot.db.model.Columns;
import io.jboot.web.directive.annotation.JFinalDirective;
import io.jboot.web.directive.base.JbootDirectiveBase;

import java.util.List;

/**
 * @program: parent
 * @description:  查找所有的分类
 * @author: Pan
 * @create: 2020-03-21 12:45
 **/
@JFinalDirective("LinkManagesTypeAll")
public class LinkManageTypeAllDirective extends JbootDirectiveBase {

    @Inject
    private JpressAddonLinkTypeService service;

    @Override
    public void onRender(Env env, Scope scope, Writer writer) {
        List<JpressAddonLinkType> all = service.findAll();
        scope.setLocal("LinkTypes", all);
        renderBody(env, scope, writer);
    }

    @Override
    public boolean hasEnd() {
        return true;
    }
}
