package cn.ucuser.addon.linkManage.directive;

import cn.ucuser.addon.linkManage.model.JpressAddonLinkType;
import cn.ucuser.addon.linkManage.service.JpressAddonLinkTypeService;
import com.jfinal.aop.Inject;
import com.jfinal.template.Env;
import com.jfinal.template.io.Writer;
import com.jfinal.template.stat.Scope;
import io.jboot.web.directive.annotation.JFinalDirective;
import io.jboot.web.directive.base.JbootDirectiveBase;

/**
 * @program: parent
 * @description:
 * @author: Pan
 * @create: 2020-03-21 13:52
 **/
@JFinalDirective("LinkManageTypeById")
public class LinkManageTypeIdDirective extends JbootDirectiveBase {

    @Inject
    private JpressAddonLinkTypeService service;

    @Override
    public void onRender(Env env, Scope scope, Writer writer) {
        Object id = getPara("type", scope);
        JpressAddonLinkType firstByColumns = service.findById(id);
        scope.setLocal("LinkType", firstByColumns);
        renderBody(env, scope, writer);
    }

    @Override
    public boolean hasEnd() {
        return true;
    }
}
