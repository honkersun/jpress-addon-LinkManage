package cn.ucuser.addon.linkManage.service.provider;

import io.jboot.aop.annotation.Bean;
import cn.ucuser.addon.linkManage.service.JpressAddonLinkListService;
import cn.ucuser.addon.linkManage.model.JpressAddonLinkList;
import io.jboot.service.JbootServiceBase;

@Bean
public class JpressAddonLinkListServiceProvider extends JbootServiceBase<JpressAddonLinkList> implements JpressAddonLinkListService {

}