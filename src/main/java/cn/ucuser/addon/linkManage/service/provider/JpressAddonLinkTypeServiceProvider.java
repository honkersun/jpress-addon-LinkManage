package cn.ucuser.addon.linkManage.service.provider;

import io.jboot.aop.annotation.Bean;
import cn.ucuser.addon.linkManage.service.JpressAddonLinkTypeService;
import cn.ucuser.addon.linkManage.model.JpressAddonLinkType;
import io.jboot.service.JbootServiceBase;

@Bean
public class JpressAddonLinkTypeServiceProvider extends JbootServiceBase<JpressAddonLinkType> implements JpressAddonLinkTypeService {

}