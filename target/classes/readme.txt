# 这是一个友情链接管理插件，后续更新请前往 <a target="_blank" href="http://ucuser.cn">浩瀚博客</a> 查看

# 插件食用方法：
### 在html页面 只有 `网站状态` 是 `通过`的才可以展示
    ```html
        #LinkManages()
            #for(linkType : LinkTypes)
        	    分类：#(linkType.name)
        		#LinkList(linkType.id)
        		    #for(link : Links)
        			    链接：#(link.title)
        			#end
        		#end
            #end
        #end
    ```
### link下的所有属性
    ```html
    link :
        id
        title           标题
        description     描述
        logo            logo图片
        url             链接
        time            添加时间
        name            网站作者名称
        email           网站联系邮箱
        remark          备注
    ```
### linkType下的所有属性
    ```html
    linkType ：
        id
        name            分类名
        description     分类描述
        icon            分类图标,你可以食用ico图片也可也食用图片地址，按模板需求来
    ```