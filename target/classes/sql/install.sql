create table if not exists `jpress_addon_link_type`
(
    `id`          int primary key auto_increment comment 'ID',
    `name`        varchar(20)  not null comment '分类名称',
    `description` varchar(255) null comment '分类描述',
    `icon`        varchar(50)  null comment '分类图标'
) comment '网站分类';

insert into `jpress_addon_link_type` values (null,'默认分类','这是系统自动为您创建的分类,你可以修改它或者删除它','icon');

create table if not exists `jpress_addon_link_list`
(
    `id`          int primary key auto_increment comment 'ID',
    `title`       varchar(50)  not null comment '网站标题',
    `description` varchar(255) null comment '网站描述',
    `logo`        varchar(100) null comment '网站LOGO',
    `url`         varchar(50)  not null comment '网址',
    `type`        int          not null comment '网站分类',
    `time`        datetime     not null comment '添加时间',
    `name`        varchar(20)  null comment '称呼',
    `email`       varchar(50)  null comment '联系邮箱',
    `state`       int(1)       not null comment '网站状态(待审核，已审核，失效)',
    `remark`    varchar(255) comment '备注'
) comment '链接列表';

insert into `jpress_addon_link_list`
values (
        null,
        '浩瀚博客',
        '做一个平凡、无聊且枯燥的人',
        'logo.png',
        'http://ucuser.cn',
        1,
        '2020-03-21 00:00:00.0',
        '浩瀚',
        '2369710264@qq.com',
        1,
        '我可以做你的专属友链嘛！'
        );